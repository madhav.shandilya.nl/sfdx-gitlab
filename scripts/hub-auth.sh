#!/bin/bash

# openssl enc -aes-256-cbc -md sha256 -salt -d -in assets/server.key.enc -out assets/server.key -k $DECRYPTION_KEY -pbkdf2
# ls assets/
openssl version
sfdx force:auth:jwt:grant --setdefaultdevhubusername --clientid $HUB_CONSUMER_KEY --jwtkeyfile assets/server.key --username $HUB_SFDC_USER
